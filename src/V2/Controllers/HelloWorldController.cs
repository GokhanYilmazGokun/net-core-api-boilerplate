using Microsoft.AspNetCore.Mvc;
using Sillycore.Web.Controllers;

namespace {solutionName}.V2.Controllers
{
    [ApiVersion("2.0")]
    [ApiVersion("3.0")]
    [Route("api")]
    public class HelloWorldController : SillyController
    {
        [HttpGet]
        public string Get() => "Hello world v2!";

        [HttpGet, MapToApiVersion("3.0")]
        public string GetV3() => "Hello world v3!";
    }
}
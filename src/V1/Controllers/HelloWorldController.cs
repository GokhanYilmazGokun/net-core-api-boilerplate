using Microsoft.AspNetCore.Mvc;
using Sillycore.Web.Controllers;

namespace {solutionName}.Controllers
{
    [ApiVersion("1.0", Deprecated = true )]
    [Route("api")]
    public class HelloWorldController : SillyController
    {
        [HttpGet("healthcheck")]
        public IActionResult Healthcheck() => Ok();
        [HttpGet]
        public string Get() => "Hello world!";
    }    
}